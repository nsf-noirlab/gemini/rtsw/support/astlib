#ifndef ASTSYSHDEF
#define ASTSYSHDEF

/*
**  - - - - - - - - -
**   a s t s y s . h
**  - - - - - - - - -
**
**  TCS coordinate transformation library internal
**
**  D.L.Terrett & P.T.Wallace   11 April 1997
**
**  Copyright RAL 1997.  All rights reserved.
*/

#define PI    3.1415926535897932384626433832795
#define PI2   (PI*2.0)
#define D90   (PI/2.0)
#define AS2R  (PI/648000.0)
#define S2R   (PI/43200.0)
#define R2D   (180.0/PI)

#endif
